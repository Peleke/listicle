import {database} from './firebase'
import React, {Component} from 'react'
import './App.css'

class App extends Component {
  state = {
    title: '',
    note: ''
  }
  handleChange = event => {
    this.setState({[event.target.name]: event.target.value})
  }
  handleSubmit = event => {
    event.preventDefault()
    event.stopPropagation()

    database
      .ref('/notes')
      .push(Object.assign(this.state, {
        createdAt: Number(Date.now())
      }))
  }
  render() {
    return (
      <div className="App container">
        <div className="row">
          <div className="col-md-12">
            <form>
              <div className="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" className="form-control" placeholder="Note Title" onChange={this.handleChange} />
              </div>
              <div className="form-group">
                <label for="note">Note</label>
                <textarea name="note" className="form-control" id="note-textarea" rows="3" onChange={this.handleChange} ></textarea>
              </div>
              <button className="btn btn-primary col-md-12" onClick={this.handleSubmit}>Submit</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default App
